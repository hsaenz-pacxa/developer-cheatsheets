# Git Cheatsheet

## Remove a Directory from Git Repository

Remove directory from git but NOT local

```bash
git rm -r one-of-the-directories
git commit -m "Remove duplicated directory"
git push origin <your-git-branch> (typically 'master', but not always)
```
