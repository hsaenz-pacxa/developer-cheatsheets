# Software

* Sublime Text
* Python - https://www.python.org/downloads/release/python-370/
* Beautiful Soup - https://www.crummy.com/software/BeautifulSoup/bs4/doc/#installing-beautiful-soup
* Selenium
* Notepad++
* Ansible
* Capistrano
* Vagrant


# Sublime Text 3
----------------
## Installation

Install [Sublime Text](https://www.sublimetext.com/3)

**Package Control**

Install [Package Control](https://packagecontrol.io/installation)

**Useful Plugins**

* [Sublime SFTP](https://wbond.net/sublime_packages/sftp/installation) - FTP, FTPS and SFTP support for Sublime Test 2 & 3

## Packages for better Markdown experience

From article [How to Set Up Sublime Text for a Vastly Better Markdown Writing Experience](https://blog.mariusschulz.com/2014/12/16/how-to-set-up-sublime-text-for-a-vastly-better-markdown-writing-experience)

**Install Monokai Extended**

* Select "Install Package" from the Command Palette: `Ctrl+Shift+P` on Windows and Linux or `Shift+Command+P` on OS X)
* Search for **"Monokai Extended"** and click `enter`.

**Switch Themes**

* Open a Markdown file in Sublime Text and make sure that syntax highlighting is set to Markdown Extended (not Markdown) `View -> Syntax -> Markdown Extended`.
* Then go to `Preferences -> Color Scheme -> Monokai Extended` and pick a theme.

**Install Markdown Extended Package**

* Select "Install Package" from the Command Palette: `Ctrl+Shift+P` on Windows and Linux or `Shift+Command+P` on OS X)
* Search for **"Markdown Extended"** and click `enter`.

**Activate this Language**

* After installing this package, open a markdown file and swith the language to `Markdown Extended`, using one of the following methods:
	* Select from the list of supported languages in your status bar at the bottom right corner of your editor
    * `ctrl + shift + p` and search for "Markdown Extended"

**Make "Markdown Extended" the default**

To make `Markdown Extended` the default highlighting for the current extension:

1. Open a file with the extension you want to set a default for (i.e. `.md`)
2. Navigate through the following menus in Sublime Text: `view -> Syntax -> Open all with current extension as... -> Markdown Extended`



# Python
--------


# Beautiful Soup
---------------


# Selenium
----------


#Notepad++
----------




